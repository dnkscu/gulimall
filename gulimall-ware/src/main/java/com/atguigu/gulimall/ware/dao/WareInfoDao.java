package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author dunikai
 * @email 1420018826@qq.com
 * @date 2023-02-25 11:13:34
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
