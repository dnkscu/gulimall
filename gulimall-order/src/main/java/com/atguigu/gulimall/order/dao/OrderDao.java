package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author dunikai
 * @email 1420018826@qq.com
 * @date 2023-02-25 11:04:20
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
